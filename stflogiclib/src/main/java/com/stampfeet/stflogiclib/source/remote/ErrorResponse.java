package com.stampfeet.stflogiclib.source.remote;

import com.google.gson.annotations.SerializedName;
import com.stampfeet.stflogiclib.models.response.BaseResponse;


public class ErrorResponse extends BaseResponse {

    @SerializedName("errors")
    private CustomError error;

    public CustomError getError() {
        return error;
    }

    public void setError(CustomError error) {
        this.error = error;
    }
}
