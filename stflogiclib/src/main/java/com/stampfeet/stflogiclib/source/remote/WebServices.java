package com.stampfeet.stflogiclib.source.remote;

import com.google.gson.JsonObject;
import com.stampfeet.stflogiclib.models.*;
import com.stampfeet.stflogiclib.models.request.*;
import com.stampfeet.stflogiclib.models.response.*;

import java.util.HashMap;

import io.reactivex.Observable;
import retrofit2.http.*;

public interface WebServices {

    //USER AUTHENTIFICATION

    @POST(Urls.SIGN_UP)
    Observable<UserAuthResponse> signup(
            @Body CreateUserRequest body);

    @POST(Urls.LOGIN)
    Observable<UserAuthResponse> login(
            @Body LoginRequest body);

    @POST(Urls.FORGOT_PASSWORD)
    Observable<BaseResponse> forgotPassword(
            @Body ForgotPasswordRequest body);

    @POST(Urls.CHANGE_PASSWORD)
    Observable<BaseResponse> changePassword(
            @Path(Urls.ID_USER) int userId,
            @Body ChangePasswordRequest body);

    //BANNERS

    @GET(Urls.BANNERS)
    Observable<BannersResponse> getBanners(
            @Path(Urls.ID_USER) int userId,
            @Query(Urls.ID_PROGRAM) int programId);

    @GET(Urls.BANNERS_OPEN)
    Observable<BannersResponse> getBannersOpen();

    //LOCATIONS

    @GET(Urls.LOCATIONS)
    Observable<PagedResponse<Branch>> getLocations(
            @Query("latitude") double latitude,
            @Query("longitude") double longitude,
            @Query("pageNumber") int pageNumber,
            @Query("pageSize") int pageSize,
            @Query("orderBy") String orderBy,
            @Query("sortAscending") boolean sortAscending,
            @Query("program") Integer programId,
            @Query("name") String programName);

    //PLANS

    @GET(Urls.PLANS)
    Observable<PlansResponse> getUserPlans(
            @Path(Urls.ID_USER) int userId);

    @GET(Urls.PLANS)
    Observable<PlansResponse> getUserPlans(
            @Path(Urls.ID_USER) int userId,
            @Query("programId") Integer programId);

    //PROMOTIONS

    @GET(Urls.PROMOTIONS)
    Observable<PromotionsResponse> getPromotions(
            @Path(Urls.ID_USER) int userId,
            @Query("latitude") double latitude,
            @Query("longitude") double longitude);


    @POST(Urls.REDEEM)
    Observable<GiftCardRedeemResponse> redeemGiftCard(
            @Path(Urls.ID_USER) int userId,
            @Body GiftCardRedeemRequest body);


    //USER DATA

    @GET(Urls.USER_TIERS)
    Observable<UserTiersResponse> getUserTiers(
            @Path(Urls.ID_USER) int userId);

    @GET(Urls.IN_APP_MESSAGES)
    Observable<InAppMessageResponse> inAppMessage(
            @Path(Urls.ID_USER) int userId,
            @Query("businessId") int businessId);

    @POST(Urls.UPDATE_CREDIT)
    Observable<CreditUpdateResponse> creditUpdate(
            @Path(Urls.ID_USER) int userId,
            @Body CreditUpdateRequest body);

    @POST(Urls.REDEEM_CARD_POINTS)
    Observable<BaseResponse> redeemCardPoints(
            @Path(Urls.ID_USER) int userId,
            @Body RedeemCardPointsRequest body);

    @PUT(Urls.UPDATE_DEVICE)
    Observable<BaseResponse> updateDevice(
            @Path(Urls.ID_USER) int userId,
            @Body UpdateDeviceRequest body);

    @PUT(Urls.UPDATE_LOCATION)
    Observable<BaseResponse> updateLocation(
            @Path(Urls.ID_USER) int userId,
            @Body HashMap<String, Object> body);


    @GET(Urls.GET_PROGRAMS)
    Observable<ProgramsResponse> getPrograms();

    @PATCH(Urls.SET_PROGRAM)
    Observable<BaseResponse> setProgram(
            @Path(Urls.ID_USER) int userId,
            @Body SetProgramRequest body);


    @POST(Urls.CREATE_EXTERNAL_CARD)
    Observable<BaseResponse> addExternalCard(
            @Path(Urls.ID_USER) int userId,
            @Body LinkedCardRequest body);

    @DELETE(Urls.DELETE_EXTERNAL_CARD)
    Observable<BaseResponse> deleteExternalCard(
            @Path(Urls.ID_USER) int userId,
            @Path(Urls.ID_CARD) int cardId);

    @GET(Urls.GET_EXTERNAL_CARDS)
    Observable<ExternalCardResponse> getExternalCards(
            @Path(Urls.ID_USER) int userId);


    //USER LOGIN DATA

    @GET(Urls.USER)
    Observable<UserAuthResponse> getUser(
            @Path(Urls.ID_USER) int userId);

    @PATCH(Urls.USER)
    Observable<BaseResponse> updateUser(
            @Path(Urls.ID_USER) int userId,
            @Body EditUserRequest body);

    @PATCH(Urls.USER)
    Observable<BaseResponse> updateMobility(
            @Path(Urls.ID_USER) int userId,
            @Body JsonObject body);

    @GET(Urls.TRANSACTIONS)
    Observable<PagedResponse<Transaction>> getTransactions(
            @Path(Urls.ID_USER) int userId,
            @Query("pageNumber") int pageNumber,
            @Query("pageSize") int pageSize,
            @Query("programId") int programId);

    //USER MESSAGE

    @POST(Urls.SEND_MESSAGE_TO_BUSINESS)
    Observable<BaseResponse> sendMessageToBusiness(
            @Body MessageToBusinessRequest body);

    @POST(Urls.SEND_POINTS_MESSAGE_TO_BUSINESS)
    Observable<BaseResponse> sendPointsMessageToBusiness(
            @Body MissingPointsMessageRequest body);

    @POST(Urls.ADD_FEEDBACK)
    Observable<FeedbackResponse> addFeedback(
            @Body Feedback body);

    @GET(Urls.FEEDBACK_PROPERTIES)
    Observable<FeedbackPropertiesResponse> getFeedbackProperties(
            @Path(Urls.ID_BUSINESS) int businessId);

    @GET(Urls.GET_FEEDBACK)
    Observable<BaseResponse> getFeedback(
            @Path(Urls.ID_STAMP) int stampId);


    //ADDRESS

    @GET(Urls.GET_ADDRESS)
    Observable<AddressByPostcodeResponse> getAddressByPostCode(
            @Query("postCode") String postCode);


    //FISHBOWL

    @GET(Urls.FISHBOWL_PROMOTIONS)
    Observable<BaseResponse> getFishbowlPromotions(
            @Path(Urls.ID_USER) int userId);


    //POSTS

    @GET(Urls.POSTS)
    Observable<PagedResponse<Post>> getPosts(
            @Query("pageNumber") int pageNumber,
            @Query("pageSize") int pageSize,
            @Query("programId") int programId);


    //PRIZEDRAW

    @GET(Urls.PRIZEDRAWS)
    Observable<PagedContentResponse<PrizeCompetition>> getPrizeDraws(
            @Query("pageNumber") int pageNumber,
            @Query("pageSize") int pageSize,
            @Query("programId") int programId);

    @GET(Urls.PRIZEDRAW_LEVELS)
    Observable<PrizeDrawLevelsResponse> getPrizeDrawLevels(
            @Path(Urls.ID_USER) int userId,
            @Path(Urls.ID_PRIZEDRAW) int prizeDrawId);

    @GET(Urls.PRIZEDRAW_WINS)
    Observable<PagedContentResponse<PrizeDraw>> getPrizeDrawWins(
            @Path(Urls.ID_USER) int userId,
            @Query("pageNumber") int pageNumber,
            @Query("pageSize") int pageSize);

    @GET(Urls.REWARDS)
    Observable<PagedResponse<Reward>> getRewards(
            @Path(Urls.ID_USER) int userId,
            @Query("pageNumber") int pageNumber,
            @Query("pageSize") int pageSize,
            @Query("mcRewardsGallery") boolean mcRewardsGallery,
            @Query("includeExpired") boolean includeExpired);

    @POST(Urls.REDEEM_REWARD)
    Observable<BaseResponse> redeemReward(
            @Path(Urls.ID_USER) int userId,
            @Body RedeemRewardRequest body);



//    //ALOHA
//
////    TODO: NO AUTH
//    @POST("aloha/signup/")
//    Observable<UserAuthResponse> alohaSignup(
//            @Body HashMap<String, Object> parameters);
//
//    @PATCH("aloha/{userId}/")
//    Observable<BaseResponse> alohaUpdateUser(
//            @Path(Urls.ID_USER) int userId,
//            @Body HashMap<String, Object> parameters);
//
//    @GET("aloha/{userId}/getUserPlans")
//    Observable<AlohaPlansResponse> alohaGetUserPlans(
//            @Path(Urls.ID_USER) int userId);

}
