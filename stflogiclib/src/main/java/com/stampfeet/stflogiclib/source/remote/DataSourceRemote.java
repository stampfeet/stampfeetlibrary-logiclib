package com.stampfeet.stflogiclib.source.remote;

import android.os.Build;
import com.google.gson.JsonObject;
import com.stampfeet.stflogiclib.models.*;
import com.stampfeet.stflogiclib.models.request.*;
import com.stampfeet.stflogiclib.models.response.*;
import com.stampfeet.stflogiclib.source.local.SessionManager;
import com.stampfeet.stflogiclib.utils.DeviceUtils;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import java.util.HashMap;
import java.util.List;


public class DataSourceRemote {

    private static int businessId;

    public static void init(String appKey, String baseUrl, int businessId) {
        RequestManager.init(appKey, baseUrl);
        DataSourceRemote.businessId = businessId;
    }

    //    To have it updated
    private static Integer getUserId() {
        Session session = SessionManager.getInstance().getSession();
        if (session != null && session.getUserid() != null) {
            return session.getUserid();
        } else {
            return null;
        }
    }

    public static Observable<UserAuthResponse> signUp(CreateUserRequest user) {
        user.setHomeProgram(null);
        return RequestManager.getApi().signup(user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(response -> {
                    SessionManager.getInstance().saveSession(response.getSession());
                    if (response.getUser().getHomeProgram() != null) {
                        SessionManager.getInstance().saveProgram(response.getUser().getHomeProgram().getId());
                    }
                    RequestManager.buildSecureApi(response.getSession().getSessionkey());
                    return response;
                });
    }

    public static Observable<UserAuthResponse> login(LoginRequest request) {
        return RequestManager.getApi().login(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(response -> {
                    SessionManager.getInstance().saveSession(response.getSession());
                    if (response.getUser().getHomeProgram() != null) {
                        SessionManager.getInstance().saveProgram(response.getUser().getHomeProgram().getId());
                    }
                    RequestManager.buildSecureApi(response.getSession().getSessionkey());
                    return response;
                });
    }

    public static void logout() {
        SessionManager.getInstance().removeSession();
        RequestManager.destroySecureApi();
    }

    public static Observable<BaseResponse> forgotPassword(ForgotPasswordRequest request) {
        return RequestManager.getApi().forgotPassword(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<BaseResponse> changePassword(ChangePasswordRequest request) {
        return RequestManager.getSecureApi().changePassword(getUserId(), request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }


    public static Observable<UserTiersResponse> getUserTiers() {
        return RequestManager.getSecureApi().getUserTiers(getUserId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<InAppMessageResponse> getInAppMessage() {
        return RequestManager.getSecureApi().inAppMessage(getUserId(), businessId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<CreditUpdateResponse> creditUpdate(CreditUpdateRequest body) {
        return RequestManager.getSecureApi().creditUpdate(getUserId(), body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<Boolean> redeemCardPoints(RedeemCardPointsRequest body) {
        return RequestManager.getSecureApi().redeemCardPoints(getUserId(), body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(response -> response.getStatus().isOk());
    }

    public static Observable<BaseResponse> updateDevice(String oneSignalId) {

        UpdateDeviceRequest deviceInfo = new UpdateDeviceRequest();
        deviceInfo.setPlatform("Android");
        deviceInfo.setDevice(DeviceUtils.getDeviceName());
        deviceInfo.setOsversion(Build.VERSION.RELEASE);
        deviceInfo.setAppid("1234567890");
        deviceInfo.setOneSignalId(oneSignalId);

        return RequestManager.getSecureApi().updateDevice(getUserId(), deviceInfo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<BaseResponse> updateLocation(double latitude, double longitude) {

        HashMap<String, Object> body = new HashMap<>();
        body.put("latitude", latitude);
        body.put("longitude", longitude);

        return RequestManager.getSecureApi().updateLocation(getUserId(), body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<List<HomeProgram>> getPrograms() {
        return RequestManager.getApi().getPrograms().map(ProgramsResponse::getPrograms)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<BaseResponse> setProgram(int programId) {
        SetProgramRequest body = new SetProgramRequest();
        body.setHomeProgram(programId);
        return RequestManager.getSecureApi().setProgram(getUserId(), body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<BaseResponse> addExternalCard(LinkedCardRequest linkedCardRequest) {
        return RequestManager.getSecureApi().addExternalCard(getUserId(), linkedCardRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<BaseResponse> deleteExternalCard(int externalCardId) {
        return RequestManager.getSecureApi().deleteExternalCard(getUserId(), externalCardId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<ExternalCardResponse> getExternalCards() {
        return RequestManager.getSecureApi().getExternalCards(getUserId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<UserAuthResponse> getUser() {
        return RequestManager.getSecureApi().getUser(getUserId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<BaseResponse> updateUser(EditUserRequest user) {
        //user.setHomeProgram(null);
        return RequestManager.getSecureApi().updateUser(user.getId(), user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<BaseResponse> updateMobility(User user) {

        JsonObject userProfile = new JsonObject();
        userProfile.addProperty("mobility", user.getUserProfile().isMobility());
        JsonObject body = new JsonObject();
        body.add("userProfile", userProfile);

        return RequestManager.getSecureApi().updateMobility(user.getId(), body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<BaseResponse> sendMessageToBusiness(MessageToBusinessRequest message) {
        return RequestManager.getSecureApi().sendMessageToBusiness(message)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<BaseResponse> sendPointsMessageToBusiness(MissingPointsMessageRequest message) {
        return RequestManager.getSecureApi().sendPointsMessageToBusiness(message)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<FeedbackPropertiesResponse> getFeedbackProperties(int businessId) {
        return RequestManager.getSecureApi().getFeedbackProperties(businessId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<AddressByPostcodeResponse> getAddressByPostCode(String postCode) {
        return RequestManager.getApi().getAddressByPostCode(postCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<FeedbackResponse> addFeedback(Feedback feedback) {
        return RequestManager.getSecureApi().addFeedback(feedback)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<BannersResponse> getBanners() {
        Integer userId = getUserId();
        int programId = SessionManager.getInstance().getProgram();
        Observable<BannersResponse> request = (userId != null) ?
                RequestManager.getSecureApi().getBanners(userId, programId) :
                RequestManager.getApi().getBannersOpen();

        return request
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<PagedResponse<Branch>> getLocations(double latitude, double longitude, int pageNumber, int pageSize) {
        return getLocations(latitude, longitude, pageNumber, pageSize, null, null, null);
    }

    /**
     *
     * @param latitude
     * @param longitude
     * @param pageNumber
     * @param pageSize
     * @param programId To support multi city on miRewards
     * @return
     */
    public static Observable<PagedResponse<Branch>> getLocations(double latitude, double longitude, int pageNumber, int pageSize, Integer programId, String programName, String orderBy) {
        return RequestManager.getApi().getLocations(latitude, longitude, pageNumber, pageSize, orderBy, true, programId, programName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<PlansResponse> getUserPlans() {
        return RequestManager.getSecureApi().getUserPlans(getUserId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<PlansResponse> getUserPlan(int programId) {
        return RequestManager.getSecureApi().getUserPlans(getUserId(), programId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<PromotionsResponse> getPromotions(double latitude, double longitude) {
        return RequestManager.getSecureApi().getPromotions(getUserId(), latitude, longitude)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<GiftCardRedeemResponse> redeemGiftCard(GiftCardRedeemRequest body) {
        return RequestManager.getSecureApi().redeemGiftCard(getUserId(), body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<PagedResponse<Post>> getPosts(int pageNumber, int pageSize ) {
        int programId = SessionManager.getInstance().getProgram();
        return RequestManager.getSecureApi().getPosts(pageNumber, pageSize, programId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<PagedResponse<Transaction>> getTransactions(int pageNumber, int pageSize, int programId) {
        return RequestManager.getSecureApi().getTransactions(getUserId(), pageNumber, pageSize, programId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<PagedContentResponse<PrizeCompetition>> getPrizeDraws(int pageNumber, int pageSize, int programId) {
        return RequestManager.getSecureApi().getPrizeDraws(pageNumber, pageSize, programId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<PrizeDrawLevelsResponse> getPrizeDrawLevels(int prizeDrawId) {
        return RequestManager.getSecureApi().getPrizeDrawLevels(getUserId(), prizeDrawId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<PagedContentResponse<PrizeDraw>> getPrizeDrawWins(int pageNumber, int pageSize) {
        return RequestManager.getSecureApi().getPrizeDrawWins(getUserId(), pageNumber, pageSize)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<PagedResponse<Reward>> getRewards(int pageNumber, int pageSize) {
        return RequestManager.getSecureApi().getRewards(getUserId(), pageNumber, pageSize, false, false)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<Boolean> redeemReward(RedeemRewardRequest body) {
        return RequestManager.getSecureApi().redeemReward(getUserId(), body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(response -> response.getStatus().isOk());
    }

    public static Observable<InitialLoadBundle> loadInitialData(double latitude, double longitude, String oneSignalId) {
        Session session = SessionManager.getInstance().getSession();
        if (session == null || session.getUserid() == null) {
            return getBanners().map(bannersResponse -> {
                InitialLoadBundle bundle = new InitialLoadBundle();
                bundle.setBanners(bannersResponse.getBanners());
                return bundle;
            });
        }

        RequestManager.buildSecureApi(session.getSessionkey());

//        Update Endpoints
        Observable<Boolean> updateInfo = Observable.zip(
                updateDevice(oneSignalId),
                updateLocation(latitude, longitude),
                (br1, br2) -> br1.getStatus().getErrorCode() == 0 && br2.getStatus().getErrorCode() == 0);

//        Get Info Endpoints
        Observable<InitialLoadBundle> bundleInfo = Observable.zip(
                getUserTiers(),
                getUserPlans(),
                getBanners(),
                getInAppMessage(),
                (userTiersResponse, plansResponse, bannersResponse, inAppMessageResponse) -> {
                    InitialLoadBundle bundle = new InitialLoadBundle();
                    bundle.setLoyaltyTiers(userTiersResponse.getLoyaltyTiers());
                    bundle.setPlans(plansResponse.getPlans());
                    bundle.setBanners(bannersResponse.getBanners());
                    bundle.setMessage(inAppMessageResponse.getMessage());
                    return bundle;
                });

        return updateInfo
                .filter(Boolean::booleanValue)
                .flatMap(isOk -> bundleInfo);

    }

}
