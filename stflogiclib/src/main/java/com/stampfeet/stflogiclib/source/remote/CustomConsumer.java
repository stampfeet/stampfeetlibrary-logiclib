package com.stampfeet.stflogiclib.source.remote;

import com.google.gson.Gson;
import io.reactivex.functions.Consumer;
import okhttp3.ResponseBody;
import retrofit2.HttpException;

import java.io.IOException;
import java.net.SocketTimeoutException;

public interface CustomConsumer extends Consumer<Throwable> {

    @Override
    default void accept(Throwable e) {
        if (e instanceof HttpException) {
            ResponseBody responseBody = ((HttpException) e).response().errorBody();
            try {
                ErrorResponse response = new Gson().fromJson(responseBody.string(), ErrorResponse.class);
                String detail = response.getError().getDetail().toString();
                run(response.getError());
            } catch (Exception ex) {
                run(new CustomError(e.getMessage()));
            }
        } else if (e instanceof SocketTimeoutException) {
            run(new CustomError(e.getMessage()));
        } else if (e instanceof IOException) {
            run(new CustomError(e.getMessage()));
        } else {
            run(new CustomError(e.getMessage()));
        }
    }

    void run(CustomError error);
}
