package com.stampfeet.stflogiclib.source.remote;

import com.google.gson.Gson;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.HttpException;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeUnit;


public class RequestManager {

    private static final String INIT_ERROR = "CustomError: You must first initialize 'RequestManager'";
    private static WebServices webServices;
    private static WebServices secureWebService;
    private static String baseUrl;
    private static String appKey;
    private static Retrofit retrofit;

    static void init(String appKey, String baseUrl) {
        RequestManager.appKey = appKey;
        RequestManager.baseUrl = baseUrl;
    }

    private static OkHttpClient getOkHttpClient(String token) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS);

        builder.addInterceptor(chain -> {
            Request.Builder requestBuilder = chain.request().newBuilder();
            requestBuilder.addHeader("appKey", appKey);
            if (token != null) requestBuilder.addHeader("sessionKey", token);
            return chain.proceed(requestBuilder.build());
        });

        return builder.build();
    }

    static WebServices getApi() {
        if (baseUrl == null) throw new RuntimeException(INIT_ERROR);

        if (webServices == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(getOkHttpClient(null))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            webServices = retrofit.create(WebServices.class);
        }
        return webServices;
        //return getMockWS();
    }

    /**
     * This method should be called after every LOGIN to update the @token
     *
     * @param token
     */
    static void buildSecureApi(String token) {
        if (baseUrl == null) throw new RuntimeException(INIT_ERROR);

        secureWebService = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(getOkHttpClient(token))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(WebServices.class);
    }

    static void destroySecureApi() {
        secureWebService = null;
    }

    static WebServices getSecureApi() {
        if (secureWebService == null) throw new RuntimeException("You must login first");
        return secureWebService;
    }

    public static String parseError(Throwable e) {

        if (e instanceof HttpException) {
            ResponseBody responseBody = ((HttpException) e).response().errorBody();
            try {
                ErrorResponse response = new Gson().fromJson(responseBody.string(), ErrorResponse.class);
                String detail = response.getError().getDetail();
                if (detail != null && !detail.isEmpty()) {
                    return detail;
                } else {
                    return e.getMessage();
                }
            } catch (Exception ex) {
                return e.getMessage();
            }
        } else if (e instanceof SocketTimeoutException) {
            return e.getMessage();
        } else if (e instanceof IOException) {
            return e.getMessage();
        } else {
            return e.getMessage();
        }
    }
}
