package com.stampfeet.stflogiclib.source.remote;

import com.google.gson.annotations.SerializedName;


public class CustomError {

    @SerializedName("id")
    private Integer id;
    @SerializedName("detail")
    private String detail;

    public CustomError(String detail) {
        this.id = 0;
        this.detail = detail;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
