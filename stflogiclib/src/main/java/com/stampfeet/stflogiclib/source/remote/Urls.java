package com.stampfeet.stflogiclib.source.remote;

public class Urls {

    public static final String ID_USER = "userId";
    public static final String ID_CARD = "cardId";
    public static final String ID_BUSINESS = "businessId";
    public static final String ID_STAMP = "stampId";
    public static final String ID_PRIZEDRAW = "prizeDraw";
    public static final String ID_PROGRAM = "programId";

    public static final String SIGN_UP = "user/signup";
    public static final String LOGIN = "user/login";
    public static final String FORGOT_PASSWORD = "user/forgotPassword";
    public static final String CHANGE_PASSWORD = "user/{" + ID_USER + "}/changePassword";

    public static final String BANNERS = "promo/{" + ID_USER + "}/getBanners";
    public static final String BANNERS_OPEN = "promo/getBanners";

    public static final String LOCATIONS = "promo/getLocationsList";

    public static final String PLANS = "promo/{" + ID_USER + "}/getUserPlans";

    public static final String PROMOTIONS = "promo/{" + ID_USER + "}/getPromotions";
    public static final String REDEEM = "promo/{" + ID_USER + "}/giftRedeem";
    public static final String REDEEM_CARD_POINTS = "stamp/{" + ID_USER + "}/redeem";

    public static final String USER_TIERS = "promo/{" + ID_USER + "}/getUserTiers";
    public static final String IN_APP_MESSAGES = "message/{" + ID_USER + "}/inappMessage";
    public static final String UPDATE_CREDIT = "stamp/{" + ID_USER + "}/creditUpdate";
    public static final String UPDATE_DEVICE = "user/{" + ID_USER + "}/updateDevice";
    public static final String UPDATE_LOCATION = "user/{" + ID_USER + "}/updateLocation";

    public static final String GET_PROGRAMS = "programs";
    public static final String SET_PROGRAM = "programs/user/{" + ID_USER + "}";

    public static final String CREATE_EXTERNAL_CARD = "user/{" + ID_USER + "}/externalCard";
    public static final String DELETE_EXTERNAL_CARD = "user/{" + ID_USER + "}/externalCard/{" + ID_CARD + "}";
    public static final String GET_EXTERNAL_CARDS = "user/{" + ID_USER + "}/linkedCards";

    public static final String USER = "user/{" + ID_USER + "}";

    public static final String SEND_MESSAGE_TO_BUSINESS = "message/sendMessageToBusiness";
    public static final String ADD_FEEDBACK = "message/addFeedback";
    public static final String FEEDBACK_PROPERTIES = "business/{" + ID_BUSINESS + "}/properties";
    public static final String GET_FEEDBACK = "message/feedback/{" + ID_STAMP + "}";

    public static final String TRANSACTIONS = "stamp/{" + ID_USER + "}/transactionLog";

    public static final String GET_ADDRESS = "getAddress";

    public static final String FISHBOWL_PROMOTIONS = "fishbowl/{" + ID_USER + "}/promotions";

    public static final String POSTS = "posts";

    public static final String PRIZEDRAWS =  "promo/prizedraws";
    public static final String PRIZEDRAW_LEVELS = "promo/{" + ID_USER + "}/prizedraws/{" + ID_PRIZEDRAW + "}";
    public static final String PRIZEDRAW_WINS = "promo/{" + ID_USER + "}/prizedraws/wins";
    public static final String REWARDS = "promo/{" + ID_USER + "}/rewards";

    public static final String REDEEM_REWARD = "stamp/{" + ID_USER + "}/redeemByReward";

    public static final String SEND_POINTS_MESSAGE_TO_BUSINESS = "message/sendMissingTxToBusiness";
}
