package com.stampfeet.stflogiclib.source.local;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.gson.Gson;
import com.stampfeet.stflogiclib.models.Session;


public class SessionManager {

    private static final String INIT_ERROR = "CustomError: You must first initialize 'SessionManager'";
    private static final String PREF_SESSION = "PREF_SESSION";
    private static final String PREF_PROGRAM = "PREF_PROGRAM";
    private SharedPreferences pref;

    private static SessionManager INSTANCE = null;

    public static void init(Context context, String preferenceName) {
        INSTANCE = new SessionManager(context, preferenceName);
    }

    public static SessionManager getInstance() {
        if (INSTANCE == null) throw new RuntimeException(INIT_ERROR);
        return INSTANCE;
    }

    private SessionManager(Context context, String preferenceName) {
        pref = context.getSharedPreferences(preferenceName.concat("_STF_LOGIC_LIB"), Context.MODE_PRIVATE);
    }

    public void saveSession(Session session) {
        Editor editor = pref.edit();
        editor.putString(PREF_SESSION, new Gson().toJson(session));
        editor.apply();
    }

    public void saveProgram(int programId) {
        Editor editor = pref.edit();
        editor.putInt(PREF_PROGRAM, programId);
        editor.apply();
    }

    public void removeSession() {
        Editor editor = pref.edit();
        editor.remove(PREF_SESSION);
        editor.remove(PREF_PROGRAM);
        editor.apply();
    }

    public Session getSession() {
        String sessionString = pref.getString(PREF_SESSION, "");
        return new Gson().fromJson(sessionString, Session.class);
    }

    public int getProgram() {
        return pref.getInt(PREF_PROGRAM, 0);
    }
}