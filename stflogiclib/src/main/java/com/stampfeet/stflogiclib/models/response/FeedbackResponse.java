package com.stampfeet.stflogiclib.models.response;

import com.google.gson.annotations.SerializedName;
import com.stampfeet.stflogiclib.models.Stamp;

import java.util.List;

public class FeedbackResponse extends BaseResponse {

    @SerializedName("stamps_given")
    private int stampsGiven;

    @SerializedName("stamps")
    private List<Stamp> stamps;

    public int getStampsGiven() {
        return stampsGiven;
    }

    public void setStampsGiven(int stampsGiven) {
        this.stampsGiven = stampsGiven;
    }

    public List<Stamp> getStamps() {
        return stamps;
    }

    public void setStamps(List<Stamp> stamps) {
        this.stamps = stamps;
    }
}