package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PageContent<T> {

    @SerializedName("content")
    private List<T> content;
    @SerializedName("numberOfElements")
    private int numberOfElements;
    @SerializedName("totalElements")
    private int totalNumberOfElements;
    @SerializedName("first")
    private boolean isFirst;
    @SerializedName("totalPages")
    private int totalPagesNumber;
    @SerializedName("number")
    private int pageNumber;
    @SerializedName("last")
    private boolean isLast;

    public int getNumberOfElements() {
        return numberOfElements;
    }

    public void setNumberOfElements(int numberOfElements) {
        this.numberOfElements = numberOfElements;
    }

    public int getTotalNumberOfElements() {
        return totalNumberOfElements;
    }

    public void setTotalNumberOfElements(int totalNumberOfElements) {
        this.totalNumberOfElements = totalNumberOfElements;
    }

    public boolean getIsFirst() {
        return isFirst;
    }

    public void setIsFirst(boolean isFirst) {
        this.isFirst = isFirst;
    }

    public int getTotalPagesNumber() {
        return totalPagesNumber;
    }

    public void setTotalPagesNumber(int totalPagesNumber) {
        this.totalPagesNumber = totalPagesNumber;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public boolean getIsLast() {
        return isLast;
    }

    public void setIsLast(boolean isLast) {
        this.isLast = isLast;
    }

    public List<T> getContent() {
        return content;
    }

    public void setContent(List<T> content) {
        this.content = content;
    }
}
