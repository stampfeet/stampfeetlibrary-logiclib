package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PrizeDraw implements Serializable {

    @SerializedName("prizedrawName")
    private String prizeDrawName;
    @SerializedName("prizeName")
    private String prizeName;

    public String getPrizeDrawName() {
        return prizeDrawName;
    }

    public void setPrizeDrawName(String prizeDrawName) {
        this.prizeDrawName = prizeDrawName;
    }

    public String getPrizeName() {
        return prizeName;
    }

    public void setPrizeName(String prizeName) {
        this.prizeName = prizeName;
    }
}
