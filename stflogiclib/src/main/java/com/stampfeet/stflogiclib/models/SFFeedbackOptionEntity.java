package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;


public class SFFeedbackOptionEntity {

    @SerializedName("index")
    private int index;
    @SerializedName("name")
    private String name;
    @SerializedName("isSelected")
    private boolean isSelected;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
