package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;


public class SFAlohaLoyaltyPlanEntity {

    @SerializedName("id") private String id;
    @SerializedName("alohaId") private String alohaId;
    @SerializedName("name") private String name;
    @SerializedName("description1") private String description1;
    @SerializedName("description2") private String description2;
    @SerializedName("imageUrl") private String imageUrl;
    @SerializedName("requiredPoints") private String requiredPoints;
    @SerializedName("currentStamps") private String currentStamps;
    @SerializedName("currentPoints") private String currentPoints;
    @SerializedName("nextReward") private String nextReward;
    @SerializedName("requiredStamps") private String requiredStamps;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAlohaId() {
        return alohaId;
    }

    public void setAlohaId(String alohaId) {
        this.alohaId = alohaId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription1() {
        return description1;
    }

    public void setDescription1(String description1) {
        this.description1 = description1;
    }

    public String getDescription2() {
        return description2;
    }

    public void setDescription2(String description2) {
        this.description2 = description2;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getRequiredPoints() {
        return requiredPoints;
    }

    public void setRequiredPoints(String requiredPoints) {
        this.requiredPoints = requiredPoints;
    }

    public String getCurrentStamps() {
        return currentStamps;
    }

    public void setCurrentStamps(String currentStamps) {
        this.currentStamps = currentStamps;
    }

    public String getCurrentPoints() {
        return currentPoints;
    }

    public void setCurrentPoints(String currentPoints) {
        this.currentPoints = currentPoints;
    }

    public String getNextReward() {
        return nextReward;
    }

    public void setNextReward(String nextReward) {
        this.nextReward = nextReward;
    }

    public String getRequiredStamps() {
        return requiredStamps;
    }

    public void setRequiredStamps(String requiredStamps) {
        this.requiredStamps = requiredStamps;
    }
}
