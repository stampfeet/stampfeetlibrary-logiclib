package com.stampfeet.stflogiclib.models.request;

import com.google.gson.annotations.SerializedName;

public class MissingPointsMessageRequest {
    @SerializedName("userId")
    private int userid;
    @SerializedName("subject")
    private String subject;
    @SerializedName("branchId")
    private String branchId;
    @SerializedName("to")
    private String to;
    @SerializedName("linkedCardId")
    private String linkedCardId;
    @SerializedName("timestamp")
    private String timestamp;
    @SerializedName("amount")
    private String amount;
    @SerializedName("msg")
    private String msg;
    @SerializedName("device")
    private String device;

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getLinkedCardId() {
        return linkedCardId;
    }

    public void setLinkedCardId(String linkedCardId) {
        this.linkedCardId = linkedCardId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

}