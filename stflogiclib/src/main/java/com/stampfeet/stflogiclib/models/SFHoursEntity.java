package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;


public class SFHoursEntity {

    @SerializedName("branchId")
    private int branchId;
    @SerializedName("fromDay")
    private String fromDay;
    @SerializedName("toDay")
    private String toDay;
    @SerializedName("fromHour")
    private String fromHour;
    @SerializedName("toHour")
    private String toHour;

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public String getFromDay() {
        return fromDay;
    }

    public void setFromDay(String fromDay) {
        this.fromDay = fromDay;
    }

    public String getToDay() {
        return toDay;
    }

    public void setToDay(String toDay) {
        this.toDay = toDay;
    }

    public String getFromHour() {
        return fromHour;
    }

    public void setFromHour(String fromHour) {
        this.fromHour = fromHour;
    }

    public String getToHour() {
        return toHour;
    }

    public void setToHour(String toHour) {
        this.toHour = toHour;
    }
}
