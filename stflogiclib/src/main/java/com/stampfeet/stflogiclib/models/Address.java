package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;

public class Address {

    @SerializedName("line_4")
    private String line4;
    @SerializedName("line_3")
    private String line3;
    @SerializedName("county")
    private String county;
    @SerializedName("area")
    private String area;
    @SerializedName("district")
    private String district;
    @SerializedName("locality")
    private String locality;
    @SerializedName("town_or_city")
    private String townOrCity;
    @SerializedName("line_2")
    private String line2;
    @SerializedName("line_1")
    private String line1;

    public String getLine4() {
        return line4;
    }

    public void setLine4(String line4) {
        this.line4 = line4;
    }

    public String getLine3() {
        return line3;
    }

    public void setLine3(String line3) {
        this.line3 = line3;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getTownOrCity() {
        return townOrCity;
    }

    public void setTownOrCity(String townOrCity) {
        this.townOrCity = townOrCity;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }
}
