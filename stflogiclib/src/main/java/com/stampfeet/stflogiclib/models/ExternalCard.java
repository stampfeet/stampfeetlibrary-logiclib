package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;

public class ExternalCard {

    @SerializedName("id")
    private Integer id;
    @SerializedName("last_numbers")
    private String lastNumbers;
    @SerializedName("scheme")
    private String scheme;
    @SerializedName("type")
    private String type;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLastNumbers() {
        return lastNumbers;
    }

    public void setLastNumbers(String lastNumbers) {
        this.lastNumbers = lastNumbers;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
