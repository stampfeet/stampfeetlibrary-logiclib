package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class User {

    @SerializedName("id")
    private int id;
    @SerializedName("email")
    private String email;
    @SerializedName("userName")
    private String userName;
    @SerializedName("lastName")
    private String lastName;
    @SerializedName("firstName")
    private String firstName;
    @SerializedName("fullName")
    private String fullName;
    @SerializedName("userProfile")
    private UserProfile userProfile = new UserProfile();
    @SerializedName("isActive")
    private boolean isActive;
    @SerializedName("userComms")
    private List<UserComm> userComms;
    @SerializedName("home_program")
    private HomeProgram homeProgram;
    @SerializedName("signUpDate")
    private String signUpDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public List<UserComm> getUserComms() {
        return userComms;
    }

    public void setUserComms(List<UserComm> userComms) {
        this.userComms = userComms;
    }

    public HomeProgram getHomeProgram() {
        return homeProgram;
    }

    public void setHomeProgram(HomeProgram homeProgram) {
        this.homeProgram = homeProgram;
    }

    public String getSignUpDate() {
        return signUpDate;
    }

    public void setSignUpDate(String signUpDate) {
        this.signUpDate = signUpDate;
    }

}
