package com.stampfeet.stflogiclib.models.response;

import com.google.gson.annotations.SerializedName;
import com.stampfeet.stflogiclib.models.Page;
import com.stampfeet.stflogiclib.models.PageContent;


public class PagedContentResponse<T> extends BaseResponse {

    @SerializedName("page")
    PageContent<T> page;

    public PageContent<T> getPage() {
        return page;
    }

    public void setPage(PageContent<T> page) {
        this.page = page;
    }
}
