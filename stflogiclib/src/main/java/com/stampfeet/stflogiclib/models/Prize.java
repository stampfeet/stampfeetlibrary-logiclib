package com.stampfeet.stflogiclib.models;

import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Prize implements Serializable {

    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("position")
    private int position;
    @SerializedName("quantity")
    private int quantity;
    @SerializedName("state")
    private String state;
    @SerializedName("customData")
    private JsonElement customData;
    private String imageURL;
    private String description;
    private String subtitle;
    private String sponsorImageURL;
    private String sponsorName;
    private String sponsorLink;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPosition() {
        return position;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getState() {
        return state;
    }

    public JsonElement getCustomData() {
        return customData;
    }

    public String getImageURL() {
        imageURL = "";
        try {
            if (customData != null) {
                JsonElement urlObject = customData.getAsJsonObject().get("prizeImageURL");
                if (urlObject != null) {
                    imageURL = urlObject.getAsString();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imageURL;
    }

    public String getDescription() {
        description = "";
        try {
            if (customData != null) {
                JsonElement urlObject = customData.getAsJsonObject().get("prizeLdonDesc");
                if (urlObject != null) {
                    description = urlObject.getAsString();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return description;
    }

    public String getSubtitle() {
        subtitle = "";
        try {
            if (customData != null) {
                JsonElement urlObject = customData.getAsJsonObject().get("subtitle");
                if (urlObject != null) {
                    subtitle = urlObject.getAsString();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return subtitle;
    }

    public String getSponsorImageURL() {
        sponsorImageURL = "";
        try {
            if (customData != null) {
                JsonElement urlObject = customData.getAsJsonObject().get("additionalPrizeImageURLs");
                if (urlObject != null) {
                    sponsorImageURL = urlObject.getAsString();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sponsorImageURL;
    }

    public String getSponsorName() {
        sponsorName = "";
        try {
            if (customData != null) {
                JsonElement urlObject = customData.getAsJsonObject().get("sponsorName");
                if (urlObject != null) {
                    sponsorName = urlObject.getAsString();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sponsorName;
    }

    public String getSponsorLink() {
        sponsorLink = "";
        try {
            if (customData != null) {
                JsonElement urlObject = customData.getAsJsonObject().get("sponsorLink");
                if (urlObject != null) {
                    sponsorLink = urlObject.getAsString();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sponsorLink;
    }
}
