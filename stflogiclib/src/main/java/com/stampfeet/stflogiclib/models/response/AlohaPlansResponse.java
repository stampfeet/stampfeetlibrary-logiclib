package com.stampfeet.stflogiclib.models.response;

import com.google.gson.annotations.SerializedName;
import com.stampfeet.stflogiclib.models.SFAlohaUserPlanEntity;


public class AlohaPlansResponse extends BaseResponse {

    @SerializedName("plans") private SFAlohaUserPlanEntity plans;

    public SFAlohaUserPlanEntity getPlans() {
        return plans;
    }

    public void setPlans(SFAlohaUserPlanEntity plans) {
        this.plans = plans;
    }
}
