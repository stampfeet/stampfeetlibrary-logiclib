package com.stampfeet.stflogiclib.models;

import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Reward implements Serializable {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("state")
    private String state;
    @SerializedName("cardId")
    private int cardId;
    @SerializedName("fromDate")
    private String fromDate;
    @SerializedName("toDate")
    private String toDate;
    @SerializedName("redeemType")
    private String redeemType;
    @SerializedName("shortDescription")
    private String shortDescription;
    @SerializedName("longDescription")
    private String longDescription;
    @SerializedName("redemptionInstructions")
    private String redemptionInstructions;
    @SerializedName("imageURL")
    private String imageURL;
    @SerializedName("imageURL2")
    private String imageURL2;
    @SerializedName("pointsPrice")
    private int pointsPrice;
    @SerializedName("customData")
    private JsonElement customData;
    private String terms;


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getCardId() {
        return cardId;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getRedeemType() {
        return redeemType;
    }

    public void setRedeemType(String redeemType) {
        this.redeemType = redeemType;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public String getRedemptionInstructions() {
        return redemptionInstructions;
    }

    public void setRedemptionInstructions(String redemptionInstructions) {
        this.redemptionInstructions = redemptionInstructions;
    }

    public String getPostRedemptionInstructions() {
        String instructions = redemptionInstructions;
        try {
            if (customData != null) {
                JsonElement urlObject = customData.getAsJsonObject().get("postRedemptionInstructions");
                if (urlObject != null) {
                    instructions = urlObject.getAsString();

                    instructions = instructions.replace("%CODE%", redemptionInstructions);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return instructions;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getImageURL2() {
        return imageURL2;
    }

    public void setImageURL2(String imageURL2) {
        this.imageURL2 = imageURL2;
    }

    public int getPointsPrice() {
        return pointsPrice;
    }

    public void setPointsPrice(int pointsPrice) {
        this.pointsPrice = pointsPrice;
    }

    public String getTerms() {
        terms = "";
        try {
            if (customData != null) {
                JsonElement urlObject = customData.getAsJsonObject().get("terms");
                if (urlObject != null) {
                    terms = urlObject.getAsString();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return terms;
    }
}
