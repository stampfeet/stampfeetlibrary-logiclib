package com.stampfeet.stflogiclib.models.response;

import com.google.gson.annotations.SerializedName;
import com.stampfeet.stflogiclib.models.Page;
import com.stampfeet.stflogiclib.models.PrizeLevelUser;

import java.util.ArrayList;


public class PrizeDrawLevelsResponse extends BaseResponse {

    @SerializedName("levels")
    ArrayList<PrizeLevelUser> levels;

    public ArrayList<PrizeLevelUser> getLevels() {
        return levels;
    }
}
