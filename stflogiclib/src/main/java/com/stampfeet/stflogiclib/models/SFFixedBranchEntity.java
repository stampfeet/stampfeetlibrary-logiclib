package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;


public class SFFixedBranchEntity {

    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
