package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Session {

    @SerializedName("id")
    private Integer id;
    @SerializedName("uucode")
    private String uucode;
    @SerializedName("userId")
    private Integer userid;
    @SerializedName("sessionKey")
    private String sessionkey;
    @SerializedName("alohaCardNumber")
    private String alohaCardNumber;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUucode() {
        return uucode;
    }

    public void setUucode(String uucode) {
        this.uucode = uucode;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getSessionkey() {
        return sessionkey;
    }

    public void setSessionkey(String sessionkey) {
        this.sessionkey = sessionkey;
    }

    public String getAlohaCardNumber() {
        return alohaCardNumber;
    }

    public void setAlohaCardNumber(String alohaCardNumber) {
        this.alohaCardNumber = alohaCardNumber;
    }
}
