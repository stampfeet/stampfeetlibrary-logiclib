package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class PrizeCompetition implements Serializable {

    @SerializedName("id")
    private int id;
    @SerializedName("cardId")
    private int cardId;
    @SerializedName("name")
    private String name;
    @SerializedName("startDate")
    private String startDate;
    @SerializedName("endDate")
    private String endDate;
    @SerializedName("levels")
    private ArrayList<PrizeLevel> levels;
    @SerializedName("prizeDrawTracker")
    private PrizeDrawTracker prizeDrawTracker;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public ArrayList<PrizeLevel> getLevels() {
        return levels;
    }

    public PrizeDrawTracker getPrizeDrawTracker() {
        return prizeDrawTracker;
    }

    public void setPrizeDrawTracker(PrizeDrawTracker prizeDrawTracker) {
        this.prizeDrawTracker = prizeDrawTracker;
    }
}
