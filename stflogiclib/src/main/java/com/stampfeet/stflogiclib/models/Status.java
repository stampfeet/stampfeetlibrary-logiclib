package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;


public class Status {

    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("statusDescription")
    private String statusDescription;
    @SerializedName("errorCode")
    private int errorCode;
    @SerializedName("errorDescription")
    private String errorDescription;
    @SerializedName("sucessCode")
    private int sucessCode;
    @SerializedName("sucessDescription")
    private String sucessDescription;
    @SerializedName("userLoggedIn")
    private boolean userLoggedIn;
    @SerializedName("ok")
    private boolean ok;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public int getSucessCode() {
        return sucessCode;
    }

    public void setSucessCode(int sucessCode) {
        this.sucessCode = sucessCode;
    }

    public String getSucessDescription() {
        return sucessDescription;
    }

    public void setSucessDescription(String sucessDescription) {
        this.sucessDescription = sucessDescription;
    }

    public boolean isUserLoggedIn() {
        return userLoggedIn;
    }

    public void setUserLoggedIn(boolean userLoggedIn) {
        this.userLoggedIn = userLoggedIn;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }
}
