package com.stampfeet.stflogiclib.models.request;

import com.google.gson.annotations.SerializedName;
import com.stampfeet.stflogiclib.models.HomeProgram;
import com.stampfeet.stflogiclib.models.UserComm;
import com.stampfeet.stflogiclib.models.UserProfile;

import java.util.List;

public class EditUserRequest {

    @SerializedName("id")
    private int id;
    @SerializedName("lastName")
    private String lastName;
    @SerializedName("firstName")
    private String firstName;
    @SerializedName("email")
    private String email;
    @SerializedName("fullName")
    private String fullName;
    @SerializedName("userProfile")
    private UserProfile userProfile = new UserProfile();
    @SerializedName("userComms")
    private List<UserComm> userComms;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public List<UserComm> getUserComms() {
        return userComms;
    }

    public void setUserComms(List<UserComm> userComms) {
        this.userComms = userComms;
    }
}
