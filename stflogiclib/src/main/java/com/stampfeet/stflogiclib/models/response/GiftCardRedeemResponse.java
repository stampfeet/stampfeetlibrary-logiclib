package com.stampfeet.stflogiclib.models.response;

import com.google.gson.annotations.SerializedName;

public class GiftCardRedeemResponse extends BaseResponse {

    @SerializedName("quantity")
    private long quantity;

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }
}
