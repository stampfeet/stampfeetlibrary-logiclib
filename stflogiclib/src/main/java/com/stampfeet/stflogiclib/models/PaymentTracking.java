package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class PaymentTracking implements Serializable {

    @SerializedName("fl_status")
    private boolean status;
    @SerializedName("fl_visa_state")
    private boolean visa;
    @SerializedName("fl_mc_state")
    private boolean mc;
    @SerializedName("fl_amex_state")
    private boolean amex;

    public boolean isStatus() {
        return status;
    }

    public boolean isVisa() {
        return visa;
    }

    public boolean isMc() {
        return mc;
    }

    public boolean isAmex() {
        return amex;
    }
}
