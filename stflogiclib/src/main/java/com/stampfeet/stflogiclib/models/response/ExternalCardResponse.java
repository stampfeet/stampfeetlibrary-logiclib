package com.stampfeet.stflogiclib.models.response;

import com.google.gson.annotations.SerializedName;
import com.stampfeet.stflogiclib.models.ExternalCard;
import com.stampfeet.stflogiclib.models.Session;
import com.stampfeet.stflogiclib.models.User;

import java.util.List;


public class ExternalCardResponse extends BaseResponse {

    @SerializedName("linked_cards")
    private List<ExternalCard> linkedCards;
    @SerializedName("session")
    private Session session;

    public List<ExternalCard> getLinkedCards() {
        return linkedCards;
    }

    public void setLinkedCards(List<ExternalCard> linkedCards) {
        this.linkedCards = linkedCards;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }
}
