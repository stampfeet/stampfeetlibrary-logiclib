package com.stampfeet.stflogiclib.models.response;

import com.google.gson.annotations.SerializedName;
import com.stampfeet.stflogiclib.models.SFDynamicBonusEntity;
import com.stampfeet.stflogiclib.models.Stamp;

import java.util.ArrayList;


public class CreditUpdateResponse extends BaseResponse {

    @SerializedName("dynamicbonus")
    private ArrayList<SFDynamicBonusEntity> dynamicBonuses;
    @SerializedName("stamp")
    private Stamp stamp;
    @SerializedName("current_stamps")
    private int currentStamps;

    public ArrayList<SFDynamicBonusEntity> getDynamicBonuses() {
        return dynamicBonuses;
    }

    public void setDynamicBonuses(ArrayList<SFDynamicBonusEntity> dynamicBonuses) {
        this.dynamicBonuses = dynamicBonuses;
    }

    public Stamp getStamp() {
        return stamp;
    }

    public void setStamp(Stamp stamp) {
        this.stamp = stamp;
    }

    public int getCurrentStamps() {
        return currentStamps;
    }

    public void setCurrentStamps(int currentStamps) {
        this.currentStamps = currentStamps;
    }
}

