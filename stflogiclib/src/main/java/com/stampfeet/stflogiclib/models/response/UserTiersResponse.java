package com.stampfeet.stflogiclib.models.response;

import com.google.gson.annotations.SerializedName;
import com.stampfeet.stflogiclib.models.LoyaltyTier;

import java.util.ArrayList;


public class UserTiersResponse extends BaseResponse {

    @SerializedName("loyalty_tiers")
    ArrayList<LoyaltyTier> loyaltyTiers;

    public ArrayList<LoyaltyTier> getLoyaltyTiers() {
        return loyaltyTiers;
    }

    public void setLoyaltyTiers(ArrayList<LoyaltyTier> loyaltyTiers) {
        this.loyaltyTiers = loyaltyTiers;
    }
}
