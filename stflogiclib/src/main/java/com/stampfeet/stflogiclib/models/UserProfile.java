package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;

public class UserProfile {

    @SerializedName("id")
    private int id;
    @SerializedName("language")
    private String language;
    @SerializedName("businessSector")
    private String businessSector;
    @SerializedName("employer")
    private String employer;
    @SerializedName("howHeard")
    private String howheard;
    @SerializedName("region")
    private String region;
    @SerializedName("town")
    private String town;
    @SerializedName("homeLocation2")
    private String homeLocation2;
    @SerializedName("homeLocation")
    private String homeLocation;
    @SerializedName("postalCode")
    private String postalCode;
    @SerializedName("postalCodeArea")
    private String postalCodeArea;
    @SerializedName("postalCodeCounty")
    private String postalCodeCounty;
    @SerializedName("postalCodeDistrict")
    private String postalCodeDistrict;
    @SerializedName("countryCode")
    private String countryCode;
    @SerializedName("gender")
    private String gender;
    @SerializedName("birthday")
    private String birthday;
    @SerializedName("mobility")
    private boolean mobility;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getBusinessSector() {
        return businessSector;
    }

    public void setBusinessSector(String businessSector) {
        this.businessSector = businessSector;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public String getHowheard() {
        return howheard;
    }

    public void setHowheard(String howheard) {
        this.howheard = howheard;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getHomeLocation2() {
        return homeLocation2;
    }

    public void setHomeLocation2(String homeLocation2) {
        this.homeLocation2 = homeLocation2;
    }

    public String getHomeLocation() {
        return homeLocation;
    }

    public void setHomeLocation(String homeLocation) {
        this.homeLocation = homeLocation;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPostalCodeArea() {
        return postalCodeArea;
    }

    public void setPostalCodeArea(String postalCodeArea) {
        this.postalCodeArea = postalCodeArea;
    }

    public String getPostalCodeCounty() {
        return postalCodeCounty;
    }

    public void setPostalCodeCounty(String postalCodeCounty) {
        this.postalCodeCounty = postalCodeCounty;
    }

    public String getPostalCodeDistrict() {
        return postalCodeDistrict;
    }

    public void setPostalCodeDistrict(String postalCodeDistrict) {
        this.postalCodeDistrict = postalCodeDistrict;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }


    public boolean isMobility() {
        return mobility;
    }

    public void setMobility(boolean mobility) {
        this.mobility = mobility;
    }
}
