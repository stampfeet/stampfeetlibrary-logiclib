package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class SFPlanEntity {

    @SerializedName("id")
    private int id;
    @SerializedName("card")
    private SFCardEntity card;
    @SerializedName("dynamicbonus")
    private ArrayList<SFDynamicBonusEntity> dynamicBonuses;
    @SerializedName("fixedbonus")
    private ArrayList<SFFixedBonusEntity> fixedBonuses;
    @SerializedName("card_id")
    private int cardId;
    @SerializedName("current_points")
    private double currentPoints;
    @SerializedName("current_stamps")
    private int currentStamps;
    @SerializedName("reward_credit")
    private int rewardCredit;
    @SerializedName("total_spend")
    private double totalSpend;
    @SerializedName("eligible_loyalty_discount")
    private int eligibleLoyaltyDiscount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SFCardEntity getCard() {
        return card;
    }

    public void setCard(SFCardEntity card) {
        this.card = card;
    }

    public ArrayList<SFDynamicBonusEntity> getDynamicBonuses() {
        return dynamicBonuses;
    }

    public void setDynamicBonuses(ArrayList<SFDynamicBonusEntity> dynamicBonuses) {
        this.dynamicBonuses = dynamicBonuses;
    }

    public ArrayList<SFFixedBonusEntity> getFixedBonuses() {
        return fixedBonuses;
    }

    public void setFixedBonuses(ArrayList<SFFixedBonusEntity> fixedBonuses) {
        this.fixedBonuses = fixedBonuses;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public double getCurrentPoints() {
        return currentPoints;
    }

    public void setCurrentPoints(double currentPoints) {
        this.currentPoints = currentPoints;
    }

    public int getCurrentStamps() {
        return currentStamps;
    }

    public void setCurrentStamps(int currentStamps) {
        this.currentStamps = currentStamps;
    }

    public double getTotalSpend() {
        return totalSpend;
    }

    public void setTotalSpend(double totalSpend) {
        this.totalSpend = totalSpend;
    }

    public int getEligibleLoyaltyDiscount() {
        return eligibleLoyaltyDiscount;
    }

    public void setEligibleLoyaltyDiscount(int eligibleLoyaltyDiscount) {
        this.eligibleLoyaltyDiscount = eligibleLoyaltyDiscount;
    }

    public int getRewardCredit() {
        return rewardCredit;
    }

    public void setRewardCredit(int rewardCredit) {
        this.rewardCredit = rewardCredit;
    }
}
