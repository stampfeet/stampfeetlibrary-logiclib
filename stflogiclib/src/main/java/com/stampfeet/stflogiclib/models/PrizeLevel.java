package com.stampfeet.stflogiclib.models;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class PrizeLevel implements Serializable {

    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("position")
    private int position;
    @SerializedName("quantity")
    private int quantity;
    @SerializedName("state")
    private String state;
    @SerializedName("customData")
    private JsonElement customData;
    @SerializedName("prizes")
    private ArrayList<Prize> prizes;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPosition() {
        return position;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getState() {
        return state;
    }

    public JsonElement getCustomData() {
        return customData;
    }

    public ArrayList<Prize> getPrizes() {
        return prizes;
    }
}
