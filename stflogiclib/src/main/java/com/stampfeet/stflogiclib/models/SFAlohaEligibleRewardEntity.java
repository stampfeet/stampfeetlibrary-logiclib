package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;


public class SFAlohaEligibleRewardEntity {

    @SerializedName("id") private String id;
    @SerializedName("description") private String description;
    @SerializedName("imageUrl") private String imageUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
