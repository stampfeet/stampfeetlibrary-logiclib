package com.stampfeet.stflogiclib.models.response;

import com.google.gson.annotations.SerializedName;
import com.stampfeet.stflogiclib.models.SFPromotionEntity;

import java.util.ArrayList;


public class PromotionsResponse extends BaseResponse {

    @SerializedName("promotions_list")
    private ArrayList<SFPromotionEntity> promotions;

    public ArrayList<SFPromotionEntity> getPromotions() {
        return promotions;
    }

    public void setPromotions(ArrayList<SFPromotionEntity> promotions) {
        this.promotions = promotions;
    }
}
