package com.stampfeet.stflogiclib.models.response;

import com.google.gson.annotations.SerializedName;
import com.stampfeet.stflogiclib.models.HomeProgram;

import java.util.List;


public class ProgramsResponse extends BaseResponse {

    @SerializedName("programs")
    private List<HomeProgram> programs;

    public List<HomeProgram> getPrograms() {
        return programs;
    }

    public void setPrograms(List<HomeProgram> programs) {
        this.programs = programs;
    }
}
