package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Post {

    @SerializedName("categories_colors")
    private List<String> categoriesColors;
    @SerializedName("categories_names")
    private List<String> categoriesNames;
    @SerializedName("short_description")
    private String shortDescription;
    @SerializedName("link")
    private String link;
    @SerializedName("date")
    private String date;
    @SerializedName("image")
    private String image;
    @SerializedName("title")
    private String title;
    @SerializedName("id")
    private String id;

    public List<String> getCategoriesColors() {
        return categoriesColors;
    }

    public void setCategoriesColors(List<String> categoriesColors) {
        this.categoriesColors = categoriesColors;
    }

    public List<String> getCategoriesNames() {
        return categoriesNames;
    }

    public void setCategoriesNames(List<String> categoriesNames) {
        this.categoriesNames = categoriesNames;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
