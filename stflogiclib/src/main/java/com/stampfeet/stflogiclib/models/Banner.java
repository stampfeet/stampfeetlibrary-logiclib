package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;


public class Banner {

    @SerializedName("end_date")
    private String endDate;
    @SerializedName("message_image_url")
    private String messageImageUrl;
    @SerializedName("message_type")
    private String messageType;
    @SerializedName("message")
    private String message;
    @SerializedName("clickable_type")
    private String clickableType;
    @SerializedName("campaign_name")
    private String campaignName;
    @SerializedName("is_clickable")
    private boolean isClickable;
    @SerializedName("branch_id")
    private int branchId;
    @SerializedName("user_id")
    private int userId;
    @SerializedName("message_name")
    private String messageName;
    @SerializedName("clickable_url")
    private String clickableUrl;
    @SerializedName("id")
    private int id;
    @SerializedName("state")
    private String state;
    @SerializedName("business_id")
    private int businessId;
    @SerializedName("start_date")
    private String startDate;

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getMessageImageUrl() {
        return messageImageUrl;
    }

    public void setMessageImageUrl(String messageImageUrl) {
        this.messageImageUrl = messageImageUrl;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getClickableType() {
        return clickableType;
    }

    public void setClickableType(String clickableType) {
        this.clickableType = clickableType;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public boolean isClickable() {
        return isClickable;
    }

    public void setClickable(boolean clickable) {
        isClickable = clickable;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getMessageName() {
        return messageName;
    }

    public void setMessageName(String messageName) {
        this.messageName = messageName;
    }

    public String getClickableUrl() {
        return clickableUrl;
    }

    public void setClickableUrl(String clickableUrl) {
        this.clickableUrl = clickableUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getBusinessId() {
        return businessId;
    }

    public void setBusinessId(int businessId) {
        this.businessId = businessId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
}
