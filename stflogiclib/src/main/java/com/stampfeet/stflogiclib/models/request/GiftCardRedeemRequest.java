package com.stampfeet.stflogiclib.models.request;

import com.google.gson.annotations.SerializedName;

public class GiftCardRedeemRequest {

    @SerializedName("giftCode")
    private int giftCode;
    @SerializedName("longitude")
    private double longitude;
    @SerializedName("latitude")
    private double latitude;

    public int getGiftCode() {
        return giftCode;
    }

    public void setGiftCode(int giftCode) {
        this.giftCode = giftCode;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
}
