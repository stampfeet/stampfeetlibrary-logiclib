package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;

public class UserComm {

    @SerializedName("subscribed")
    private boolean subscribed;
    @SerializedName("description")
    private String description;
    @SerializedName("title")
    private String title;
    @SerializedName("id")
    private int id;

    public boolean getSubscribed() {
        return subscribed;
    }

    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
