package com.stampfeet.stflogiclib.models.response;

import com.google.gson.annotations.SerializedName;
import com.stampfeet.stflogiclib.models.InAppMessage;


public class InAppMessageResponse extends BaseResponse {

    @SerializedName("message")
    private InAppMessage message;

    public InAppMessage getMessage() {
        return message;
    }

    public void setMessage(InAppMessage message) {
        this.message = message;
    }
}
