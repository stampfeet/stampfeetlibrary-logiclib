package com.stampfeet.stflogiclib.models.request;

import com.google.gson.annotations.SerializedName;

public class RedeemRewardRequest {

    @SerializedName("rewardId")
    private int rewardId;
    @SerializedName("quantity")
    private int quantity;
    @SerializedName("longitude")
    private int longitude;
    @SerializedName("latitude")
    private int latitude;

    public int getRewardId() {
        return rewardId;
    }
    public void setRewardId(int rewardId) {
        this.rewardId = rewardId;
    }

    public int getQuantity() {
        return quantity;
    }
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getLongitude() {
        return longitude;
    }
    public void setLongitude(int longitude) {
        this.longitude = longitude;
    }

    public int getLatitude() {
        return latitude;
    }
    public void setLatitude(int latitude) {
        this.latitude = latitude;
    }

}
