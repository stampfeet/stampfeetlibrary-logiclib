package com.stampfeet.stflogiclib.models;

import com.stampfeet.stflogiclib.models.response.BaseResponse;

import java.util.ArrayList;
import java.util.List;


public class InitialLoadBundle extends BaseResponse {

    private List<LoyaltyTier> loyaltyTiers;
    private List<SFPlanEntity> plans;
    private InAppMessage message;
    private List<Banner> banners;
    private boolean isEmpty;

    public InitialLoadBundle() {
        loyaltyTiers = new ArrayList<>();
        plans = new ArrayList<>();
        banners = new ArrayList<>();
        message = null;
        isEmpty = true;
    }

    public List<LoyaltyTier> getLoyaltyTiers() {
        return loyaltyTiers;
    }

    public void setLoyaltyTiers(List<LoyaltyTier> loyaltyTiers) {
        this.isEmpty = false;
        this.loyaltyTiers = loyaltyTiers;
    }

    public List<SFPlanEntity> getPlans() {
        return plans;
    }

    public void setPlans(List<SFPlanEntity> plans) {
        this.isEmpty = false;
        this.plans = plans;
    }

    public List<Banner> getBanners() {
        return banners;
    }

    public void setBanners(List<Banner> banners) {
        this.isEmpty = false;
        this.banners = banners;
    }

    public InAppMessage getMessage() {
        return message;
    }

    public void setMessage(InAppMessage message) {
        this.isEmpty = false;
        this.message = message;
    }

    public boolean isEmpty(){
        return isEmpty;
    }
}
