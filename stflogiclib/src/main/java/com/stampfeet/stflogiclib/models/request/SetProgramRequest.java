package com.stampfeet.stflogiclib.models.request;

import com.google.gson.annotations.SerializedName;

public class SetProgramRequest {

    @SerializedName("home_program")
    private int homeProgram;

    public int getHomeProgram() {
        return homeProgram;
    }

    public void setHomeProgram(int homeProgram) {
        this.homeProgram = homeProgram;
    }
}
