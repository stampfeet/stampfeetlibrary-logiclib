package com.stampfeet.stflogiclib.models.request;

import com.google.gson.annotations.SerializedName;

public class LinkedCardRequest {

    @SerializedName("expYear")
    private String expyear;
    @SerializedName("lastNumbers")
    private String lastnumbers;
    @SerializedName("scheme")
    private String scheme;
    @SerializedName("expMonth")
    private String expmonth;
    @SerializedName("expDate")
    private String expdate;
    @SerializedName("type")
    private String type = "Fidel";
    @SerializedName("card")
    private String card;


    public String getExpyear() {
        return expyear;
    }

    public void setExpyear(String expyear) {
        this.expyear = expyear;
    }

    public String getLastnumbers() {
        return lastnumbers;
    }

    public void setLastnumbers(String lastnumbers) {
        this.lastnumbers = lastnumbers;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getExpmonth() {
        return expmonth;
    }

    public void setExpmonth(String expmonth) {
        this.expmonth = expmonth;
    }

    public String getExpdate() {
        return expdate;
    }

    public void setExpdate(String expdate) {
        this.expdate = expdate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }
}
