package com.stampfeet.stflogiclib.models.request;

import com.google.gson.annotations.SerializedName;

public class RedeemCardPointsRequest {

    @SerializedName("longitude")
    private double longitude;
    @SerializedName("latitude")
    private double latitude;
    @SerializedName("amountRedeem")
    private int amountredeem;
    @SerializedName("card")
    private int card;
    @SerializedName("type")
    private String type;
    @SerializedName("id")
    private int id;

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public int getAmountredeem() {
        return amountredeem;
    }

    public void setAmountredeem(int amountredeem) {
        this.amountredeem = amountredeem;
    }

    public int getCard() {
        return card;
    }

    public void setCard(int card) {
        this.card = card;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
