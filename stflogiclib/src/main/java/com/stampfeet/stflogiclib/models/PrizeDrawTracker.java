package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;

public class PrizeDrawTracker {

    @SerializedName("entries")
    private int entries;
    @SerializedName("promotions")
    private float promotions;
    @SerializedName("spent")
    private float spent;
    @SerializedName("users")
    private int users;

    public int getEntries() {
        return entries;
    }

    public void setEntries(int entries) {
        this.entries = entries;
    }

    public float getPromotions() {
        return promotions;
    }

    public void setPromotions(float promotions) {
        this.promotions = promotions;
    }

    public float getSpent() {
        return spent;
    }

    public void setSpent(float spent) {
        this.spent = spent;
    }

    public int getUsers() {
        return users;
    }

    public void setUsers(int users) {
        this.users = users;
    }

}
