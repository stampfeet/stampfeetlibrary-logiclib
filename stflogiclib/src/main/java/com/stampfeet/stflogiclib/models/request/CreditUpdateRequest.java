package com.stampfeet.stflogiclib.models.request;

import com.google.gson.annotations.SerializedName;

public class CreditUpdateRequest {

    @SerializedName("longitude")
    private double longitude;
    @SerializedName("latitude")
    private double latitude;
    @SerializedName("cardId")
    private int cardid;
    @SerializedName("isRedeem")
    private boolean isredeem;
    @SerializedName("code")
    private String code;
    @SerializedName("stampNum")
    private int stampnum;
    @SerializedName("branchId")
    private int branchid;
    @SerializedName("businessId")
    private int businessid;

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public int getCardid() {
        return cardid;
    }

    public void setCardid(int cardid) {
        this.cardid = cardid;
    }

    public boolean getIsredeem() {
        return isredeem;
    }

    public void setIsredeem(boolean isredeem) {
        this.isredeem = isredeem;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getStampnum() {
        return stampnum;
    }

    public void setStampnum(int stampnum) {
        this.stampnum = stampnum;
    }

    public int getBranchid() {
        return branchid;
    }

    public void setBranchid(int branchid) {
        this.branchid = branchid;
    }

    public int getBusinessid() {
        return businessid;
    }

    public void setBusinessid(int businessid) {
        this.businessid = businessid;
    }
}
