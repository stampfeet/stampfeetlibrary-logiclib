package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class LoyaltyTier {

    @SerializedName("tier_movement_warning")
    private int tierMovementWarning;
    @SerializedName("from_visits")
    private int fromVisits;
    @SerializedName("is_user_tier")
    private boolean isUserTier;
    @SerializedName("image_1")
    private String image1;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("id")
    private int id;
    @SerializedName("image_2")
    private String image2;
    @SerializedName("business_id")
    private int businessId;
    @SerializedName("rewards")
    private ArrayList<SFRewardEntity> rewards;

    public int getTierMovementWarning() {
        return tierMovementWarning;
    }

    public void setTierMovementWarning(int tierMovementWarning) {
        this.tierMovementWarning = tierMovementWarning;
    }

    public int getFromVisits() {
        return fromVisits;
    }

    public void setFromVisits(int fromVisits) {
        this.fromVisits = fromVisits;
    }

    public boolean isUserTier() {
        return isUserTier;
    }

    public void setUserTier(boolean userTier) {
        isUserTier = userTier;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public int getBusinessId() {
        return businessId;
    }

    public void setBusinessId(int businessId) {
        this.businessId = businessId;
    }

    public ArrayList<SFRewardEntity> getRewards() {
        return rewards;
    }

    public void setRewards(ArrayList<SFRewardEntity> rewards) {
        this.rewards = rewards;
    }
}

