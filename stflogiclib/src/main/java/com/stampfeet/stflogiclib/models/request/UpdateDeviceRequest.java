package com.stampfeet.stflogiclib.models.request;

import com.google.gson.annotations.SerializedName;

public class UpdateDeviceRequest {

    @SerializedName("appId")
    private String appid;
    @SerializedName("device")
    private String device;
    @SerializedName("osVersion")
    private String osversion;
    @SerializedName("platform")
    private String platform;
    @SerializedName("oneSignalID")
    private String oneSignalId;

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getOsversion() {
        return osversion;
    }

    public void setOsversion(String osversion) {
        this.osversion = osversion;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getOneSignalId() {
        return oneSignalId;
    }

    public void setOneSignalId(String oneSignalId) {
        this.oneSignalId = oneSignalId;
    }
}
