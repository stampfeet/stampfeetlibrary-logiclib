package com.stampfeet.stflogiclib.models.response;

import com.google.gson.annotations.SerializedName;
import com.stampfeet.stflogiclib.models.SFPropertiesEntity;


public class FeedbackPropertiesResponse extends BaseResponse{

    @SerializedName("properties")
    private SFPropertiesEntity properties;

    public SFPropertiesEntity getProperties() {
        return properties;
    }

    public void setProperties(SFPropertiesEntity properties) {
        this.properties = properties;
    }
}
