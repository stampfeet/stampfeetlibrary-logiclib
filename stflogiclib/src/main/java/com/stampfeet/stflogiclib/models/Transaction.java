package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;


public class Transaction {

    @SerializedName("type_code")
    private int typeCode;
    @SerializedName("branch_name")
    private String branchName;
    @SerializedName("type")
    private String type;
    @SerializedName("points")
    private int points;
    @SerializedName("amount")
    private double amount;
    @SerializedName("amount_redemption")
    private double amountRedemption;
    @SerializedName("timestamp")
    private String timestamp;

    public int getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(int typeCode) {
        this.typeCode = typeCode;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public double getAmountRedemption() {
        return amountRedemption;
    }

    public void setAmountRedemption(double amountRedemption) {
        this.amountRedemption = amountRedemption;
    }
}
