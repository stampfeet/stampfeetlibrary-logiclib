package com.stampfeet.stflogiclib.models.response;

import com.google.gson.annotations.SerializedName;
import com.stampfeet.stflogiclib.models.Page;


public class PagedResponse<T> extends BaseResponse {

    @SerializedName("page")
    Page<T> page;

    public Page<T> getPage() {
        return page;
    }

    public void setPage(Page<T> page) {
        this.page = page;
    }
}
