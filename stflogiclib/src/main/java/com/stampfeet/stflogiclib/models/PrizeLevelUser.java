package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PrizeLevelUser implements Serializable {

    @SerializedName("id")
    private int id;
    @SerializedName("entries")
    private int entries;
    @SerializedName("isElegibleMinThreshold")
    private ElegibleObject isElegibleMinThreshold;
    @SerializedName("isElegibleBusinessVisits")
    private ElegibleObject iisElegibleBusinessVisitsd;
    @SerializedName("elegibleGroup")
    private boolean elegibleGroup;

    public int getId() {
        return id;
    }

    public int getEntries() {
        return entries;
    }

    public ElegibleObject getIsElegibleMinThreshold() {
        return isElegibleMinThreshold;
    }

    public ElegibleObject getIisElegibleBusinessVisitsd() {
        return iisElegibleBusinessVisitsd;
    }

    public boolean isElegibleGroup() {
        return elegibleGroup;
    }

    public class ElegibleObject implements Serializable {

        @SerializedName("missing")
        private int missing;
        @SerializedName("live")
        private boolean live;

        public int getMissing() {
            return missing;
        }

        public boolean isLive() {
            return live;
        }
    }
}