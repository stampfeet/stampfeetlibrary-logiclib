package com.stampfeet.stflogiclib.models.response;

import com.google.gson.annotations.SerializedName;
import com.stampfeet.stflogiclib.models.Session;
import com.stampfeet.stflogiclib.models.User;


public class UserAuthResponse extends BaseResponse {

    @SerializedName("user")
    private User user;
    @SerializedName("session")
    private Session session;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }
}
