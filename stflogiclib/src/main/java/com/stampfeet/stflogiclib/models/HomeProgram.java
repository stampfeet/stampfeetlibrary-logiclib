package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HomeProgram {

    @SerializedName("blog_hero_image")
    private String blogHeroImage;
    @SerializedName("giftcard_site_link")
    private String giftcardSiteLink;
    @SerializedName("mobility")
    private int mobility;
    @SerializedName("home_banner_giftcard")
    private String homeBannerGiftcard;
    @SerializedName("home_banner_image")
    private String homeBannerImage;
    @SerializedName("link_uri")
    private String linkUri;
    @SerializedName("brands_logos")
    private List<String> brandsLogos;
    @SerializedName("employers")
    private List<String> employers;
    @SerializedName("blog_link")
    private String blogLink;
    @SerializedName("insta_link")
    private String instaLink;
    @SerializedName("twitter_link")
    private String twitterLink;
    @SerializedName("facebook_link")
    private String facebookLink;
    @SerializedName("name")
    private String name;
    @SerializedName("id")
    private int id;
    @SerializedName("visible")
    private boolean isVisible;



    public String getBlogHeroImage() {
        return blogHeroImage;
    }

    public void setBlogHeroImage(String blogHeroImage) {
        this.blogHeroImage = blogHeroImage;
    }

    public String getGiftcardSiteLink() {
        return giftcardSiteLink;
    }

    public void setGiftcardSiteLink(String giftcardSiteLink) {
        this.giftcardSiteLink = giftcardSiteLink;
    }

    public int getMobility() {
        return mobility;
    }

    public void setMobility(int mobility) {
        this.mobility = mobility;
    }

    public String getHomeBannerGiftcard() {
        return homeBannerGiftcard;
    }

    public void setHomeBannerGiftcard(String homeBannerGiftcard) {
        this.homeBannerGiftcard = homeBannerGiftcard;
    }

    public String getHomeBannerImage() {
        return homeBannerImage;
    }

    public void setHomeBannerImage(String homeBannerImage) {
        this.homeBannerImage = homeBannerImage;
    }

    public String getLinkUri() {
        return linkUri;
    }

    public void setLinkUri(String linkUri) {
        this.linkUri = linkUri;
    }

    public List<String> getBrandsLogos() {
        return brandsLogos;
    }

    public void setBrandsLogos(List<String> brandsLogos) {
        this.brandsLogos = brandsLogos;
    }

    public List<String> getEmployers() {
        return employers;
    }

    public void setEmployers(List<String> employers) {
        this.employers = employers;
    }

    public String getBlogLink() {
        return blogLink;
    }

    public void setBlogLink(String blogLink) {
        this.blogLink = blogLink;
    }

    public String getInstaLink() {
        return instaLink;
    }

    public void setInstaLink(String instaLink) {
        this.instaLink = instaLink;
    }

    public String getTwitterLink() {
        return twitterLink;
    }

    public void setTwitterLink(String twitterLink) {
        this.twitterLink = twitterLink;
    }

    public String getFacebookLink() {
        return facebookLink;
    }

    public void setFacebookLink(String facebookLink) {
        this.facebookLink = facebookLink;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return name;
    }

    public boolean isVisible() {
        return isVisible;
    }
}
