package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class SFFixedBonusEntity {

    @SerializedName("branchDependent")
    private boolean branchDependent;
    @SerializedName("branches")
    private ArrayList<SFFixedBranchEntity> branches;
    @SerializedName("cardId")
    private int cardId;
    @SerializedName("days")
    private String days;
    @SerializedName("description")
    private String description;
    @SerializedName("id")
    private int id;
    @SerializedName("live")
    private boolean live;
    @SerializedName("name")
    private String name;
    @SerializedName("all_branches")
    private boolean allBranches;
    @SerializedName("from_time")
    private String fromTime;
    @SerializedName("to_time")
    private String toTime;
    @SerializedName("image_1")
    private String image1;
    @SerializedName("image_2")
    private String image2;
    @SerializedName("num_of_stamps")
    private int numOfStamps;

    public boolean isBranchDependent() {
        return branchDependent;
    }

    public void setBranchDependent(boolean branchDependent) {
        this.branchDependent = branchDependent;
    }

    public ArrayList<SFFixedBranchEntity> getBranches() {
        return branches;
    }

    public void setBranches(ArrayList<SFFixedBranchEntity> branches) {
        this.branches = branches;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isLive() {
        return live;
    }

    public void setLive(boolean live) {
        this.live = live;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAllBranches() {
        return allBranches;
    }

    public void setAllBranches(boolean allBranches) {
        this.allBranches = allBranches;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public int getNumOfStamps() {
        return numOfStamps;
    }

    public void setNumOfStamps(int numOfStamps) {
        this.numOfStamps = numOfStamps;
    }
}
