package com.stampfeet.stflogiclib.models.response;

import com.google.gson.annotations.SerializedName;
import com.stampfeet.stflogiclib.models.Address;

import java.util.ArrayList;


public class AddressByPostcodeResponse extends BaseResponse {

    @SerializedName("addresses")
    private ArrayList<Address> addresses;

    public ArrayList<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(ArrayList<Address> addresses) {
        this.addresses = addresses;
    }
}
